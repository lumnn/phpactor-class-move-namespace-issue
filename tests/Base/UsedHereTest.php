<?php

namespace App\Tests\Base;

use App\Base\UsedHere;
use PHPUnit\Framework\TestCase;

class UsedHereTest extends TestCase
{
    public function testConstructor () {
        $usedHere = new UsedHere();

        $this->assertIsObject($usedHere->foo);
        $this->assertIsObject($usedHere->bar);
    }
}
