<?php

namespace App\Base;

use App\Imported;

class UsedHere
{
    public $foo;

    public $bar;

    public function __construct()
    {
        $this->foo = new Imported\Foo();
        $this->bar = new Imported\Bar();
    }
}
